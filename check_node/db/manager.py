import asyncio
from dataclasses import dataclass, fields
from datetime import datetime, timezone
from enum import Enum
from functools import wraps
from typing import (
    Callable,
    Dict,
    List,
    Optional,
    Tuple,
    Type,
    Any,
    Generator,
    AsyncGenerator,
)

import sqlalchemy as sa
from aiomysql.sa import Engine, create_engine
from aiomysql.sa.result import ResultProxy
from pymysql import OperationalError
from sqlalchemy import Table
from sqlalchemy.dialects import mysql
from sqlalchemy.dialects.mysql import Insert
from sqlalchemy.dialects.mysql.mysqldb import MySQLCompiler_mysqldb
from sqlalchemy.sql import FromClause
from sqlalchemy.sql.ddl import CreateTable, DropTable
from sqlalchemy.sql.elements import BinaryExpression

from check_node.config import DbEnv, get_db_env, get_env
from check_node.logger import get_logger
from check_node.exceptions import ConnectionIsNotSet
from check_node.db.models import CheckFrequency, Scheme, T, URLCheckDetails

logger = get_logger(__name__)


class TableName(Enum):
    URL_CHECK_DETAILS = "url_check_details"


@dataclass(frozen=True)
class TablePair:
    table: Table
    replace_strategy_constraint: Tuple[str, ...]


class TableDict(Dict[TableName, TablePair]):
    pass


class ColumnsDict(Dict[TableName, FromClause]):
    pass


def is_engine_set(method: Callable):
    @wraps(method)
    def check_engine_set(*args, **kwargs):
        db_manager: DBManager = args[0]
        if getattr(db_manager, "_engine", None) is not None:
            return method(*args, **kwargs)
        else:
            raise ConnectionIsNotSet()

    return check_engine_set


def to_ucd_dict(item: T) -> Dict[str, Any]:
    return {
        field.name: getattr(item, field.name, None)
        for field in fields(URLCheckDetails)
    }


def batchify(iterable: List[T], n=1) -> Generator[List[T], None, None]:
    length = len(iterable)
    for ndx in range(0, length, n):
        yield iterable[ndx : min(ndx + n, length)]


def logger_executing(message: str):
    logger.info(f"executing: {message}\n")


class UTCDateTime(sa.types.TypeDecorator):
    impl = mysql.DATETIME

    def process_bind_param(self, value, engine):
        if value is not None:
            return value.astimezone(timezone.utc)

    def process_result_value(self, value, engine):
        if value is not None:
            return datetime(
                value.year,
                value.month,
                value.day,
                value.hour,
                value.minute,
                value.second,
                value.microsecond,
                tzinfo=timezone.utc,
            )


class DBManager:
    _env: DbEnv

    def __init__(self, env: Optional[DbEnv] = None):
        self._env: DbEnv = env or get_db_env()
        self._engine: Optional[Engine] = None
        self.metadata = sa.MetaData()
        self.table_dict = TableDict()
        self.c_dict = ColumnsDict()
        self._echo: bool = get_env().is_debug()
        self.BATCH_SIZE = self._env.batch_size

    async def set_engine(self, loop=None):
        if loop is None:
            loop = asyncio.get_event_loop()
        try:
            if self._engine is None:
                # todo: i hope someone will manage to correct the typing stuff for mypy with `_engine`
                self._engine = await create_engine(
                    user=self._env.MYSQL_USER,
                    host=self._env.DB_HOST,
                    port=self._env.db_port,
                    password=self._env.MYSQL_PASSWORD,
                    db=self._env.MYSQL_DATABASE,
                    loop=loop,
                    echo=self._echo,
                    autocommit=True,
                    use_unicode=True,
                    charset="utf8mb4",
                )
        except OperationalError as e:
            logger.error(exc_info=e, msg="database is not available")
            raise SystemExit("database is not available")

    @is_engine_set
    async def init_tables(self) -> None:
        # create URLCheckDetails table for model
        self.table_dict[TableName.URL_CHECK_DETAILS] = TablePair(
            sa.Table(
                TableName.URL_CHECK_DETAILS.value,
                self.metadata,
                sa.Column(
                    "id", sa.Integer, primary_key=True, autoincrement=True
                ),
                sa.Column("hostname", sa.String(256), nullable=False),
                sa.Column("scheme", sa.Enum(Scheme), nullable=False),
                sa.Column("url", sa.String(2048), nullable=False),
                sa.Column(
                    "accessible", sa.Boolean, nullable=False, default=False
                ),
                sa.Column(
                    "last_check_time_by_utc",
                    UTCDateTime(timezone=True, fsp=6),
                    nullable=False,
                ),
                sa.Column("elapsed_dns_lookup", sa.Float, nullable=False),
                sa.Column("elapsed_tls_handshake", sa.Float, nullable=False),
                sa.Column("elapsed_server_process", sa.Float, nullable=False),
                sa.Column(
                    "elapsed_content_transfer", sa.Float, nullable=False
                ),
                sa.Column(
                    "check_frequency", sa.Enum(CheckFrequency), nullable=False
                ),
                sa.Column("content_length", sa.Integer, nullable=True),
                sa.Column("status_code", sa.Integer, nullable=True),
                sa.Column("exc", sa.Text(65535), nullable=True),
                sa.UniqueConstraint(
                    "scheme", "hostname", name="_uc_scheme_hostname"
                ),
            ),
            ("scheme", "hostname"),
        )
        self.c_dict[TableName.URL_CHECK_DETAILS] = self.table_dict[
            TableName.URL_CHECK_DETAILS
        ].table.c

    @is_engine_set
    async def drop_all_tables(self):
        async with self._engine.acquire() as connection:
            for table_pair in self.table_dict.values():
                drop = DropTable(table_pair.table).compile(
                    dialect=mysql.dialect()
                )
                drop_str = str(drop).replace(
                    "DROP TABLE", "DROP TABLE IF EXISTS"
                )
                logger_executing(drop_str)
                await connection.execute(drop_str)

    @is_engine_set
    async def create_tables(self):
        # todo: i hope someone will manage to correct the typing stuff for mypy with `_engine`
        async with self._engine.acquire() as connection:  # type: ignore
            for table_pair in self.table_dict.values():
                create = CreateTable(table_pair.table).compile(
                    dialect=mysql.dialect()
                )
                create_str = str(create).replace(
                    "CREATE TABLE", "CREATE TABLE IF NOT EXISTS"
                )
                logger_executing(create_str)
                await connection.execute(create_str)

    @is_engine_set
    async def insert(
        self, table_name: TableName, data: List[T]
    ) -> List[ResultProxy]:
        # in fact is does not simply insert, it doth replace if it does find the same one
        table: Table = self.table_dict[table_name].table

        results: List[ResultProxy] = []

        if len(data) == 0:
            return results

        async with self._engine.acquire() as connection:  # type: ignore
            for sliced_data in batchify(data, self.BATCH_SIZE):
                first_item = sliced_data[0]
                first_values = to_ucd_dict(first_item)
                insert = Insert(table=table, values=first_values)
                compiled: MySQLCompiler_mysqldb = insert.compile(
                    dialect=mysql.dialect()
                )
                compiled_str = str(compiled)
                replace_str = compiled_str.replace(
                    "INSERT INTO", "REPLACE INTO"
                )

                def params_from_item(item: T):
                    ucd_dict = to_ucd_dict(item)
                    return tuple(ucd_dict.values())

                generate_params = (
                    params_from_item(item) for item in sliced_data
                )
                results.append(
                    await connection.execute(replace_str, *generate_params)
                )
                await connection.execute("COMMIT")
        return results

    async def _select(
        self,
        table_name: TableName,
        cls: Type[T],
        where_clause: Any = None,
        limit: int = 0,
        offset: int = 0,
        fetch_all: bool = True,
    ) -> List[T]:
        table = self.table_dict[table_name].table
        select = table.select()
        if where_clause is not None:
            select = select.where(where_clause)
        if not fetch_all:
            select = select.limit(limit).offset(offset)
        async with self._engine.acquire() as connection:  # type: ignore
            result = await connection.execute(select)
            return [
                cls(
                    **{
                        field.name: row.get(field.name)
                        for field in fields(cls)
                    }
                )
                for row in await result.fetchall()
            ]

    @is_engine_set
    async def select(
        self,
        table_name: TableName,
        cls: Type[T],
        where_clause: Any = None,
        limit: int = 0,
        offset: int = 0,
        fetch_all: bool = True,
    ) -> List[T]:
        return await self._select(
            table_name, cls, where_clause, limit, offset, fetch_all
        )

    @is_engine_set
    async def count(self, table_name: TableName, where_clause=None):
        table = self.table_dict[table_name].table
        count = table.count()
        if where_clause is not None:
            count = count.where(whereclause=where_clause)
        async with self._engine.acquire() as connection:  # type: ignore
            results = await connection.execute(count)
            length: int = (await results.fetchall())[0][0]
            return length

    @is_engine_set
    async def batch_select(
        self,
        table_name: TableName,
        cls: Type[T],
        where_clause: Optional[BinaryExpression] = None,
        batch_size: Optional[int] = None,
    ) -> AsyncGenerator[List[T], None]:
        length = await self.count(table_name, where_clause)

        batch_size = batch_size or self.BATCH_SIZE
        last_iter = length % batch_size
        iters_count = length // batch_size + (0 if last_iter == 0 else 1)

        for idx in range(iters_count):
            yield await self._select(
                table_name,
                cls,
                where_clause,
                last_iter if idx + 1 == iters_count else batch_size,
                idx * batch_size,
                False,
            )

    @is_engine_set
    async def delete(self, table_name: TableName, where_clause) -> ResultProxy:
        table = self.table_dict[table_name].table
        delete = table.delete(whereclause=where_clause)
        async with self._engine.acquire() as connection:  # type: ignore
            result = await connection.execute(delete)
            return result

    @property
    def engine(self):
        return self._engine
