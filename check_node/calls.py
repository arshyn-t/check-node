from typing import (
    List,
    Tuple,
    Optional,
    AsyncGenerator,
    cast,
    AsyncIterable,
    Any,
    TypeVar,
    Callable,
    Type,
)

from check_node.exceptions import UnparseableEntity
from check_node.rabbit.models import (
    InMessage,
    OutMessage,
    RabbitActionType,
    CheckFrequencyWithTimeInterval,
    CheckFrequencyWrapped,
    OUT_TYPES,
    URLWithCheckFrequency,
    HostnameWrapped,
    DeletedMatchingHostname,
)
from check_node.logger import get_logger
from check_node.db.models import TimeInterval, URLCheckDetails, CheckFrequency
from check_node.models import URLDetails
from check_node.check import CheckKit
from check_node.utils import get_datetime_now_by_utc
from check_node.db.manager import DBManager, TableName

logger = get_logger(__name__)


class MetaCalls:
    @staticmethod
    def m_set_time_interval(
        check_frequency: CheckFrequency, time_interval: TimeInterval
    ):
        check_frequency.set_interval(time_interval)

    @staticmethod
    def m_get_time_interval(check_frequency: CheckFrequency) -> TimeInterval:
        return check_frequency.get_interval()


class LightCalls(MetaCalls):
    def __init__(self, db_manager: DBManager, check_kit: CheckKit):
        self.db_manager = db_manager
        self.check_kit = check_kit
        super(LightCalls, self).__init__()

    async def l_whoami(self):
        return await self.check_kit.whoami()

    async def l_delete_matching_hostname(self, hostname: str):
        c_ucd_table = self.db_manager.c_dict[TableName.URL_CHECK_DETAILS]
        clause = c_ucd_table.hostname == hostname

        ucds = await self.db_manager.select(
            TableName.URL_CHECK_DETAILS, URLCheckDetails, clause
        )
        _ = await self.db_manager.delete(TableName.URL_CHECK_DETAILS, clause)
        return ucds

    async def l_check_also_log(
        self, url: str, check_frequency: CheckFrequency = CheckFrequency.LOW
    ) -> List[URLCheckDetails]:
        urld, ucds, was_in_interval = await self.l_was_url_in_interval(
            url, check_frequency
        )
        if not was_in_interval:
            ucds = await self.check_kit.get_url(urld, check_frequency)
            await self.log_ucds(ucds)
        return ucds

    async def l_was_url_in_interval(
        self, url: str, check_frequency: CheckFrequency = CheckFrequency.LOW
    ) -> Tuple[URLDetails, List[URLCheckDetails], bool]:

        urld = self.check_kit.parse_url(url)
        tablehostname = self.db_manager.c_dict[
            TableName.URL_CHECK_DETAILS
        ].hostname
        clause = tablehostname == urld.hostname
        results: List[URLCheckDetails] = await self.db_manager.select(
            TableName.URL_CHECK_DETAILS, URLCheckDetails, clause
        )

        if results is None or len(results) == 0:
            return urld, [], False

        for result in results:
            time_interval = self.m_get_time_interval(check_frequency)
            if (
                get_datetime_now_by_utc() - result.last_check_time_by_utc
            ) > time_interval.as_timedelta():
                return urld, [], False
        return urld, results, True

    async def log_ucds(self, ucds: List[URLCheckDetails]):
        await self.db_manager.insert(TableName.URL_CHECK_DETAILS, ucds)


class HeavyCalls(LightCalls):
    def __init__(self, db_manager: DBManager, check_kit: CheckKit):
        check_frequency = db_manager.c_dict[
            TableName.URL_CHECK_DETAILS
        ].check_frequency
        self.low_clause = check_frequency == CheckFrequency.LOW
        self.high_clause = check_frequency == CheckFrequency.HIGH
        super().__init__(db_manager, check_kit)

    async def _h_get_all_for_x_freq(
        self, clause=None, batch_size: Optional[int] = None
    ) -> AsyncGenerator[List[URLCheckDetails], None]:
        if clause is not None:
            async for entries in self.db_manager.batch_select(
                TableName.URL_CHECK_DETAILS,
                URLCheckDetails,
                clause,
                batch_size,
            ):
                yield entries
        else:
            async for entries in self.db_manager.batch_select(
                TableName.URL_CHECK_DETAILS,
                URLCheckDetails,
                batch_size=batch_size,
            ):
                yield entries

    async def _h_update_all_for_x_freq(
        self, check_frequency: CheckFrequency, batch_size: Optional[int] = None
    ):

        clause = (
            self.low_clause
            if check_frequency == CheckFrequency.LOW
            else self.high_clause
        )

        async for entries in self._h_get_all_for_x_freq(clause, batch_size):
            req_body = "\n".join([entry.url for entry in entries])
            answers = await self.check_kit.heavy_request(
                req_body, check_frequency
            )

            result = await self.db_manager.insert(
                TableName.URL_CHECK_DETAILS, answers
            )
            if result is not None:
                logger.info(
                    f"UPDATED "
                    f"{getattr(result, 'rowcount', 0)} "
                    f"ENTRIES OF {check_frequency} "
                    f"TYPE FOR {get_datetime_now_by_utc()}"
                )
            yield answers

    def update_all_for_high_freq(self) -> AsyncIterable[List[URLCheckDetails]]:
        return self._h_update_all_for_x_freq(CheckFrequency.HIGH)

    def update_all_for_low_freq(self) -> AsyncIterable[List[URLCheckDetails]]:
        return self._h_update_all_for_x_freq(CheckFrequency.LOW)

    def get_all_urls(self) -> AsyncIterable[List[URLCheckDetails]]:
        return self._h_get_all_for_x_freq()


def partial_out_message(
    asn_id: str, action: RabbitActionType
) -> Callable[[OUT_TYPES], OutMessage]:
    def func(payload: OUT_TYPES):
        return OutMessage(asn_id, action, payload)

    return func


V = TypeVar("V")


def cast_or_raise_exc(
    action: RabbitActionType, payload: Any
) -> Callable[[Type[V], Any], V]:
    def wrap_cast(cls: Type[V], obj: Any) -> V:
        if type(obj) != cls:
            raise UnparseableEntity(action, payload)
        return cast(cls, obj)  # type: ignore

    return wrap_cast


class RabbitCalls:
    def __init__(self, heavy_calls: HeavyCalls, asn_id: str):
        self.heavy_calls = heavy_calls
        self.asn_id = asn_id

    async def respond(self, in_message: InMessage) -> OutMessage:
        action, _payload = in_message.action, in_message.payload
        out_message = partial_out_message(self.asn_id, action)
        core = cast_or_raise_exc(action, _payload)
        if action == RabbitActionType.SET_TIME_INTERVAL:
            return SetTimeIntervalProcess(
                self,
                out_message,
                core(CheckFrequencyWithTimeInterval, _payload),
            )
        elif action == RabbitActionType.GET_TIME_INTERVAL:
            payload: CheckFrequencyWrapped = core(
                CheckFrequencyWrapped, _payload
            )
            check_frequency = payload.check_frequency
            return out_message(
                CheckFrequencyWithTimeInterval(
                    check_frequency,
                    self.heavy_calls.m_get_time_interval(check_frequency),
                )
            )

        elif action == RabbitActionType.WHOAMI:
            return out_message(await self.heavy_calls.l_whoami())

        elif action == RabbitActionType.GET_URL:
            upayload: URLWithCheckFrequency = core(
                URLWithCheckFrequency, _payload
            )
            url, check_frequency = upayload.url, upayload.check_frequency
            return out_message(
                await self.heavy_calls.l_check_also_log(url, check_frequency)
            )

        elif action == RabbitActionType.DELETE_MATCHING_HOSTNAME:
            hpayload: HostnameWrapped = core(HostnameWrapped, _payload)
            hostname = hpayload.hostname
            return out_message(
                DeletedMatchingHostname(
                    hostname,
                    await self.heavy_calls.l_delete_matching_hostname(
                        hostname
                    ),
                )
            )
        else:
            raise UnparseableEntity(action, _payload)

    async def heavy_respond(
        self, in_message: InMessage
    ) -> AsyncGenerator[OutMessage, None]:
        action, payload = in_message.action, in_message.payload
        out_message = partial_out_message(self.asn_id, action)
        if action == RabbitActionType.UPDATE_ALL_FOR_HIGH_FREQ:
            async for ucds in self.heavy_calls.update_all_for_high_freq():
                yield out_message(ucds)

        elif action == RabbitActionType.UPDATE_ALL_FOR_LOW_FREQ:
            async for ucds in self.heavy_calls.update_all_for_low_freq():
                yield out_message(ucds)

        elif action == RabbitActionType.GET_ALL_URLS:
            async for ucds in self.heavy_calls.get_all_urls():
                yield out_message(ucds)

        else:
            raise UnparseableEntity(action, payload)


def SetTimeIntervalProcess(
    rc: RabbitCalls,
    out_message: Callable[[OUT_TYPES], OutMessage],
    payload: CheckFrequencyWithTimeInterval,
) -> OutMessage:
    check_frequency, interval = (payload.check_frequency, payload.interval)
    rc.heavy_calls.m_set_time_interval(check_frequency, interval)
    interval = rc.heavy_calls.m_get_time_interval(check_frequency)
    return out_message(
        CheckFrequencyWithTimeInterval(check_frequency, interval)
    )
