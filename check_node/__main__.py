import asyncio
import ssl
from typing import Any, Callable, Coroutine

import aio_pika
import jsons
import uvloop
from aio_pika import RobustConnection, Channel, IncomingMessage, Queue
from aiohttp import ClientSession, TCPConnector

from check_node.calls import HeavyCalls, RabbitCalls
from check_node.check import CheckKit
from check_node.config import get_env, get_db_env
from check_node.db.manager import DBManager
from check_node.logger import get_logger
from check_node.rabbit.models import InMessage, RabbitActionType, OutMessage
from check_node.utils import get_project_root

logger = get_logger(__name__)


async def name_yourself(rc: RabbitCalls) -> OutMessage:
    return await rc.respond(InMessage(RabbitActionType.WHOAMI, None))


async def publish(channel: Channel, pub_queue_name, out_message):
    if channel.default_exchange is not None:
        return await channel.default_exchange.publish(
            aio_pika.Message(
                body=jsons.dumps(jsons.dump(out_message)).encode()
            ),
            routing_key=pub_queue_name,
        )
    raise ValueError("channel.default_exchange is None")


def process_message_wrapped(
    channel: Channel, pub_queue_name: str, rc: RabbitCalls
) -> Callable[[IncomingMessage], Coroutine[Any, Any, Any]]:
    async def process_message(pika_msg: IncomingMessage):
        msg: str = pika_msg.body.decode()
        logger.debug(f"got msg\n: {msg}")
        try:
            in_message = jsons.loads(msg, InMessage)
            if not in_message.is_heavy():
                await publish(
                    channel, pub_queue_name, await rc.respond(in_message)
                )
            else:
                async for out_message in rc.heavy_respond(in_message):
                    await publish(channel, pub_queue_name, out_message)
            await pika_msg.ack(False)
        except Exception as e:
            logger.error("got exc", exc_info=e)

    return process_message


async def graceful_shutdown(rc: RabbitCalls, connection: RobustConnection):
    rc.heavy_calls.db_manager.engine.close()
    await rc.heavy_calls.check_kit.session.close()
    await rc.heavy_calls.db_manager.engine.wait_closed()
    await connection.close()


async def main(loop):
    env = get_env()
    db_env = get_db_env()

    connector = TCPConnector(limit=env.loadbound)

    ssl_ctx = None if not env.use_crt else ssl.create_default_context(
        cafile=f'{get_project_root()}/data/gov.pem.crt')
    session = ClientSession(connector=connector)
    sem = asyncio.Semaphore(env.loadbound)
    ck = CheckKit(
        env.HEAVY_NODE_URL,
        session,
        env.request_timeout,
        env.loadbound,
        sem,
        ssl_ctx=ssl_ctx,
        verify_ssl=env.verify_ssl
    )
    db = DBManager(db_env)
    await db.set_engine()
    await db.init_tables()
    await db.create_tables()
    hc = HeavyCalls(db, ck)
    ipdets = await hc.l_whoami()
    rc = RabbitCalls(hc, ipdets.asn_id)

    connection: RobustConnection = await aio_pika.connect_robust(
        env.RABBIT_URL, loop=loop
    )
    sub_queue_name = f"check:{ipdets.asn_id}"
    pub_queue_name = "master"

    logger.info(f"SUBCRIBING INTO {sub_queue_name}")

    async with connection:
        channel: Channel = await connection.channel()

        queue: Queue

        try:
            queue = await channel.get_queue(sub_queue_name)
        except aio_pika.exceptions.ChannelClosed:
            logger.warning("queue doesn't exist, creating one")
            await channel.reopen()
            queue = await channel.declare_queue(
                sub_queue_name, auto_delete=False, durable=True
            )

        await publish(channel, pub_queue_name, await name_yourself(rc))

        async with queue.iterator() as queue_iter:
            pmw = process_message_wrapped(channel, pub_queue_name, rc)
            async for message in queue_iter:
                await pmw(message)

        return connection, rc


if __name__ == "__main__":
    uvloop.install()
    loop = asyncio.new_event_loop()
    connection, rc = loop.run_until_complete(main(loop))
    try:
        loop.run_forever()
    finally:
        loop.run_until_complete(graceful_shutdown(rc, connection))
