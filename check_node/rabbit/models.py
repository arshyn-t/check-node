from dataclasses import dataclass
from enum import auto
from typing import Union, List

from check_node.db.models import (
    AutoEnum,
    CheckFrequency,
    TimeInterval,
    URLCheckDetails,
)
from check_node.models import IPDetails


class RabbitActionType(AutoEnum):
    # meta calls
    SET_TIME_INTERVAL = auto()
    GET_TIME_INTERVAL = auto()

    # light calls
    WHOAMI = auto()
    GET_URL = auto()
    DELETE_MATCHING_HOSTNAME = auto()

    # heavy calls
    UPDATE_ALL_FOR_LOW_FREQ = auto()
    UPDATE_ALL_FOR_HIGH_FREQ = auto()
    GET_ALL_URLS = auto()


HEAVY_ACTIONS = (
    RabbitActionType.UPDATE_ALL_FOR_LOW_FREQ,
    RabbitActionType.UPDATE_ALL_FOR_HIGH_FREQ,
    RabbitActionType.GET_ALL_URLS,
)


@dataclass(frozen=True)
class CheckFrequencyWithTimeInterval:
    check_frequency: CheckFrequency
    interval: TimeInterval


@dataclass(frozen=True)
class CheckFrequencyWrapped:
    check_frequency: CheckFrequency


@dataclass(frozen=True)
class URLWithCheckFrequency:
    url: str
    check_frequency: CheckFrequency


@dataclass(frozen=True)
class HostnameWrapped:
    hostname: str


IN_TYPES = Union[
    CheckFrequencyWithTimeInterval,
    URLWithCheckFrequency,
    CheckFrequencyWrapped,
    HostnameWrapped,
    None,
]


@dataclass(frozen=True, unsafe_hash=True)
class InMessage:
    action: RabbitActionType
    payload: IN_TYPES

    def is_heavy(self):
        return self.action in HEAVY_ACTIONS


@dataclass(frozen=True)
class DeletedMatchingHostname:
    hostname: str
    deleted: List[URLCheckDetails]


OUT_TYPES = Union[
    List[URLCheckDetails],
    DeletedMatchingHostname,
    CheckFrequencyWithTimeInterval,
    IPDetails,
    None,
]


@dataclass(frozen=True, unsafe_hash=True)
class OutMessage:
    asn_id: str
    action: RabbitActionType
    payload: OUT_TYPES
