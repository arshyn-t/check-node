import asyncio
import json
from typing import Dict

import pytest
from aiohttp import ClientSession, TCPConnector
from aioresponses import aioresponses

from check_node.calls import HeavyCalls, RabbitCalls
from check_node.check import CheckKit
from check_node.config import Env, get_env
from check_node.utils import get_project_root
from check_node.db.manager import DBManager


@pytest.fixture()
def str_freqs_yaml():
    return """HIGH:
  days: 0.0
  hours: 0.0
  microseconds: 0.0
  milliseconds: 0.0
  minutes: 5.0
  seconds: 0.0
  weeks: 0.0
LOW:
  days: 0.0
  hours: 1.0
  microseconds: 0.0
  milliseconds: 0.0
  minutes: 0.0
  seconds: 0.0
  weeks: 0.0
"""


@pytest.fixture()
def temp_freqs(tmpdir, str_freqs_yaml):
    directory = tmpdir / "freqs"
    directory.mkdir()
    path = directory / ".yaml"
    path.write_text(str_freqs_yaml, encoding="utf-8")
    assert path.read_text(encoding="utf-8") == str_freqs_yaml
    return path


@pytest.fixture()
def env_dict_ok(temp_freqs) -> Dict[str, str]:
    return {
        "RABBIT_URL": "lol",
        "DEBUG": "True",
        "HEAVY_NODE_URL": "http://heavy:2346",
        "LOGGER_STDIO": "True",
        "REQUEST_TIMEOUT": "30",
        "LOADBOUND": "10000",
        "FREQS_FILE_PATH": temp_freqs.strpath,
    }


@pytest.fixture()
def env_dict_fail() -> Dict[str, str]:
    return {"RABBIT_URL": "1", "BEBUG": "TRUE"}


@pytest.fixture()
def cur_env(env_dict_ok) -> Env:
    return get_env(env_dict_ok)


@pytest.fixture()
def close_aio():
    yield None
    loop = asyncio.get_event_loop()
    if not loop.is_closed():
        loop.close()


@pytest.fixture()
def rdap_dict() -> str:
    with open(get_project_root() / "tests/test_resources.json") as resourses:
        rdap_dict = json.loads(resourses.read())["rdap"]
        return rdap_dict


@pytest.fixture
async def check_kit(cur_env) -> CheckKit:
    connector = TCPConnector(limit=cur_env.loadbound, force_close=True)
    session = ClientSession(connector=connector)
    sem = asyncio.Semaphore(cur_env.loadbound)
    return CheckKit(
        cur_env.HEAVY_NODE_URL,
        session,
        cur_env.request_timeout,
        cur_env.loadbound,
        sem,
    )


# TODO: mock http call
@pytest.fixture()
def rdap_mock(mocker, rdap_dict):
    mock = mocker.patch("ipwhois.rdap.RDAP.lookup", return_value=rdap_dict)
    return mock


@pytest.fixture()
def aio_mock_responses() -> aioresponses:
    with aioresponses() as m:
        yield m


@pytest.fixture()
async def heavy_calls(
    db_manager_create_tables: DBManager, check_kit: CheckKit
) -> HeavyCalls:
    return HeavyCalls(db_manager_create_tables, check_kit)


@pytest.fixture()
async def rabbit_calls(heavy_calls: HeavyCalls, default_asn_id) -> RabbitCalls:
    return RabbitCalls(heavy_calls, default_asn_id)
