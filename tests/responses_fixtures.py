from typing import List

import pytest
from aioresponses import aioresponses

from check_node.check import CheckKit
from check_node.config import Env
from check_node.db.models import URLCheckDetails
from check_node.models import URLDetails
from tests.insert_replace_bench import generate


@pytest.fixture()
def not_blocked_url_str() -> str:
    return "this-website-was-not-blocked-by-abayev-ga.ng"


@pytest.fixture()
def blocked_url_str() -> str:
    return "this-website-was-blocked-by-abayev-ga.ng"


@pytest.fixture()
def url_to_parse_route():
    return "/path?var=value#tag"


@pytest.fixture()
def url_to_parse_no_protocol(not_blocked_url_str, url_to_parse_route):
    return f"{not_blocked_url_str}{url_to_parse_route}"


@pytest.fixture()
def url_to_parse_http(not_blocked_url_str, url_to_parse_route):
    return f"http://{not_blocked_url_str}{url_to_parse_route}"


@pytest.fixture()
def url_to_parse_https(not_blocked_url_str, url_to_parse_route):
    return f"https://{not_blocked_url_str}{url_to_parse_route}"


@pytest.fixture()
def wrong_url_str(not_blocked_url_str, url_to_parse_route) -> str:
    return f"ftp://{not_blocked_url_str}{url_to_parse_route}"


@pytest.fixture()
def http_not_blocked_url_str(not_blocked_url_str) -> str:
    return f"http://{not_blocked_url_str}"


@pytest.fixture()
def https_not_blocked_url_str(not_blocked_url_str) -> str:
    return f"https://{not_blocked_url_str}"


@pytest.fixture()
def http_blocked_url_str(blocked_url_str) -> str:
    return f"http://{blocked_url_str}"


@pytest.fixture()
def https_blocked_url_str(blocked_url_str) -> str:
    return f"https://{blocked_url_str}"


@pytest.fixture()
def came_from_redirect_msg() -> str:
    return "u came from redirect, don't ya?"


@pytest.fixture()
async def url_details_not_blocked(
    check_kit: CheckKit, not_blocked_url_str: str
) -> URLDetails:
    return check_kit.parse_url(not_blocked_url_str)


@pytest.fixture()
async def url_details_blocked(
    check_kit: CheckKit, blocked_url_str: str
) -> URLDetails:
    return check_kit.parse_url(blocked_url_str)


@pytest.fixture()
async def response_not_blocked_301(
    aio_mock_responses: aioresponses,
    http_not_blocked_url_str,
    https_not_blocked_url_str,
):
    aio_mock_responses.head(
        http_not_blocked_url_str,
        body=f"redirecting to {https_not_blocked_url_str}",
        headers={"Content-Length": "123"},
        status=301,
    )
    return aio_mock_responses


@pytest.fixture()
async def response_not_blocked_200(
    aio_mock_responses: aioresponses,
    https_not_blocked_url_str,
    came_from_redirect_msg,
):
    aio_mock_responses.head(
        url=https_not_blocked_url_str,
        body=came_from_redirect_msg,
        headers={"Content-Length": "123"},
        status=200,
    )
    return aio_mock_responses


@pytest.fixture()
async def response_not_blocked_301_d(
    aio_mock_responses: aioresponses,
    http_not_blocked_url_str,
    https_not_blocked_url_str,
):
    aio_mock_responses.head(
        url=http_not_blocked_url_str,
        body=f"redirecting to {https_not_blocked_url_str}",
        status=301,
    )
    return aio_mock_responses


@pytest.fixture()
async def response_not_blocked_200_d(
    aio_mock_responses: aioresponses,
    https_not_blocked_url_str,
    came_from_redirect_msg,
):
    aio_mock_responses.head(
        url=https_not_blocked_url_str, body=came_from_redirect_msg, status=200
    )
    return aio_mock_responses


@pytest.fixture()
async def response_not_blocked_400(
    aio_mock_responses: aioresponses,
    https_not_blocked_url_str,
    came_from_redirect_msg,
):
    aio_mock_responses.head(
        url=https_not_blocked_url_str,
        body=came_from_redirect_msg,
        headers={"Content-Length": "123"},
        status=400,
    )
    return aio_mock_responses


@pytest.fixture()
async def response_not_blocked_500(
    aio_mock_responses: aioresponses,
    https_not_blocked_url_str,
    came_from_redirect_msg,
):
    aio_mock_responses.head(
        url=https_not_blocked_url_str,
        body=came_from_redirect_msg,
        headers={"Content-Length": "123"},
        status=500,
    )
    return aio_mock_responses


@pytest.fixture()
async def response_blocked_http(
    aio_mock_responses: aioresponses, http_blocked_url_str
):
    aio_mock_responses.head(http_blocked_url_str, timeout=True)
    return aio_mock_responses


@pytest.fixture()
async def response_blocked_https(
    aio_mock_responses: aioresponses, https_blocked_url_str
):
    aio_mock_responses.head(https_blocked_url_str, timeout=True)
    return aio_mock_responses


@pytest.fixture()
def generated_ucds_for_batch_insert() -> List[URLCheckDetails]:
    return generate(32000, True)


@pytest.fixture()
async def rabbit_response_not_blocked_301(aio_mock_responses: aioresponses):
    aio_mock_responses.head(
        url="http://www.abayev-did-nothing-wro.ng/really?I=swear&to=god",
        status=301,
    )
    return aio_mock_responses


@pytest.fixture()
async def rabbit_response_not_blocked_200(aio_mock_responses: aioresponses):
    aio_mock_responses.head(
        url="https://www.abayev-did-nothing-wro.ng/really?I=swear&to=god",
        status=200,
        headers={"Content-Length": "1337"},
    )
    return aio_mock_responses


@pytest.fixture()
def str_heavy_response() -> str:
    return """{
  "resp_ans_with_errs": [
    {
      "timeout": false,
      "resp_ans": {
        "status_code": 301,
        "content_length": 0,
        "elapsed": {
          "dns_lookup": 0.0,
          "tls_handshake": 0.0,
          "server_process": 0.0,
          "content_transfer": 0.9
        }
      },
      "url": "http://www.abayev-did-nothing-wro.ng/really?I=swear&to=god",
      "queried_when": "2020-01-01T01:01:01.000001Z",
      "err": null
    },
    {
      "timeout": false,
      "resp_ans": {
        "status_code": 200,
        "content_length": 1337,
        "elapsed": {
          "dns_lookup": 0.0,
          "tls_handshake": 0.0,
          "server_process": 0.0,
          "content_transfer": 0.9
        }
      },
      "url": "https://www.abayev-did-nothing-wro.ng/really?I=swear&to=god",
      "queried_when": "2020-01-01T01:01:01.000001Z",
      "err": null
    }
  ]
}"""


@pytest.fixture()
async def rabbit_heavy_response(
    aio_mock_responses: aioresponses, cur_env: Env, str_heavy_response: str
):
    aio_mock_responses.post(
        url=f"{cur_env.HEAVY_NODE_URL}/heavy-process?timeout={cur_env.request_timeout}&loadbound={cur_env.loadbound}",
        status=200,
        body=str_heavy_response,
    )
    return aio_mock_responses
