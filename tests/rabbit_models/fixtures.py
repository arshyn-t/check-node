import dataclasses
from datetime import datetime, timezone
from typing import List

import pytest

from check_node.db.models import (
    CheckFrequency,
    TimeInterval,
    URLCheckDetails,
    Scheme,
)
from check_node.models import IPDetails
from check_node.rabbit.models import (
    InMessage,
    RabbitActionType,
    CheckFrequencyWithTimeInterval,
    CheckFrequencyWrapped,
    URLWithCheckFrequency,
    HostnameWrapped,
    OutMessage,
    DeletedMatchingHostname,
)


@pytest.fixture()
def default_asn_id() -> str:
    return "15169"


@pytest.fixture()
def default_check_time() -> datetime:
    return datetime(
        year=2020,
        month=1,
        day=1,
        hour=1,
        minute=1,
        second=1,
        microsecond=1,
        tzinfo=timezone.utc,
    )


@pytest.fixture()
def default_ucds(default_check_time) -> List[URLCheckDetails]:
    return [
        URLCheckDetails(
            "www.abayev-did-nothing-wro.ng",
            Scheme.HTTP,
            "http://www.abayev-did-nothing-wro.ng/really?I=swear&to=god",
            True,
            default_check_time,
            0.0,
            0.0,
            0.0,
            0.9,
            CheckFrequency.LOW,
            0,
            301,
            None,
        ),
        URLCheckDetails(
            "www.abayev-did-nothing-wro.ng",
            Scheme.HTTPS,
            "https://www.abayev-did-nothing-wro.ng/really?I=swear&to=god",
            True,
            default_check_time,
            0.0,
            0.0,
            0.0,
            0.9,
            CheckFrequency.LOW,
            1337,
            200,
            None,
        ),
    ]


@pytest.fixture()
def default_high_ucds(default_ucds) -> List[URLCheckDetails]:
    default_ucds[0] = dataclasses.replace(
        default_ucds[0], check_frequency=CheckFrequency.HIGH
    )
    default_ucds[1] = dataclasses.replace(
        default_ucds[1], check_frequency=CheckFrequency.HIGH
    )
    return default_ucds


@pytest.fixture()
def default_str_ucds():
    return """[
    {
      "hostname": "www.abayev-did-nothing-wro.ng",
      "scheme": "HTTP",
      "url": "http://www.abayev-did-nothing-wro.ng/really?I=swear&to=god",
      "accessible": true,
      "last_check_time_by_utc": "2020-01-01T01:01:01Z",
      "elapsed_dns_lookup": 0.0,
      "elapsed_tls_handshake": 0.0,
      "elapsed_server_process": 0.0,
      "elapsed_content_transfer": 0.9,
      "check_frequency": "LOW",
      "content_length": 0,
      "status_code": 301,
      "exc": null
    },
    {
      "hostname": "www.abayev-did-nothing-wro.ng",
      "scheme": "HTTPS",
      "url": "https://www.abayev-did-nothing-wro.ng/really?I=swear&to=god",
      "accessible": true,
      "last_check_time_by_utc": "2020-01-01T01:01:01Z",
      "elapsed_dns_lookup": 0.0,
      "elapsed_tls_handshake": 0.0,
      "elapsed_server_process": 0.0,
      "elapsed_content_transfer": 0.9,
      "check_frequency": "LOW",
      "content_length": 1337,
      "status_code": 200,
      "exc": null
    }
  ]"""


@pytest.fixture
def in_str_set_time_interval() -> str:
    return """{
  "action": "SET_TIME_INTERVAL",
  "payload": {
    "check_frequency": "LOW",
    "interval": {
      "weeks": 0.0,
      "days": 0.0,
      "hours": 1.0,
      "minutes": 0.0,
      "seconds": 0.0,
      "microseconds": 0.0,
      "milliseconds": 0.0
    }
  }
}"""


@pytest.fixture()
def in_message_set_time_interval():
    return InMessage(
        RabbitActionType.SET_TIME_INTERVAL,
        CheckFrequencyWithTimeInterval(
            CheckFrequency.LOW, TimeInterval(hours=1.0)
        ),
    )


@pytest.fixture()
def in_str_get_time_interval():
    return """{
  "action": "GET_TIME_INTERVAL",
  "payload": {
    "check_frequency": "LOW"
  }
}"""


@pytest.fixture()
def in_message_get_time_interval():
    return InMessage(
        RabbitActionType.GET_TIME_INTERVAL,
        CheckFrequencyWrapped(CheckFrequency.LOW),
    )


@pytest.fixture()
def in_str_whoami():
    return """{
  "action": "WHOAMI"
}"""


@pytest.fixture()
def in_message_whoami():
    return InMessage(RabbitActionType.WHOAMI, None)


@pytest.fixture()
def in_str_get_url():
    return """{
  "action": "GET_URL",
  "payload": {
    "url": "https://www.abayev-did-nothing-wro.ng/really?I=swear&to=god",
    "check_frequency": "LOW"
  }
}"""


@pytest.fixture()
def in_message_get_url():
    return InMessage(
        RabbitActionType.GET_URL,
        URLWithCheckFrequency(
            "https://www.abayev-did-nothing-wro.ng/really?I=swear&to=god",
            CheckFrequency.LOW,
        ),
    )


@pytest.fixture()
def in_str_delete_matching_hostname():
    return """{
  "action": "DELETE_MATCHING_HOSTNAME",
  "payload": {
    "hostname": "www.abayev-did-nothing-wro.ng"
  }
}"""


@pytest.fixture()
def in_message_delete_matching_hostname():
    return InMessage(
        RabbitActionType.DELETE_MATCHING_HOSTNAME,
        HostnameWrapped("www.abayev-did-nothing-wro.ng"),
    )


@pytest.fixture()
def in_str_update_for_all_low_freq():
    return """{
  "action": "UPDATE_ALL_FOR_LOW_FREQ"
}"""


@pytest.fixture()
def in_message_update_for_all_low_freq():
    return InMessage(RabbitActionType.UPDATE_ALL_FOR_LOW_FREQ, None)


@pytest.fixture()
def in_str_update_for_all_high_freq():
    return """{
  "action": "UPDATE_ALL_FOR_HIGH_FREQ"
}"""


@pytest.fixture()
def in_message_update_for_all_high_freq():
    return InMessage(RabbitActionType.UPDATE_ALL_FOR_HIGH_FREQ, None)


@pytest.fixture()
def in_str_get_all():
    return """{
  "action": "GET_ALL_URLS"
}"""


@pytest.fixture()
def in_message_get_all():
    return InMessage(RabbitActionType.GET_ALL_URLS, None)


@pytest.fixture()
def out_message_set_time_interval(default_asn_id):
    return OutMessage(
        default_asn_id,
        RabbitActionType.SET_TIME_INTERVAL,
        CheckFrequencyWithTimeInterval(
            CheckFrequency.LOW, TimeInterval(hours=1)
        ),
    )


@pytest.fixture()
def out_str_set_time_interval(default_asn_id):
    return (
        """{
  "asn_id": \""""
        f"{default_asn_id}"
        """\",
  "action": "SET_TIME_INTERVAL",
  "payload": {
    "check_frequency": "LOW",
    "interval": {
      "weeks": 0.0,
      "days": 0.0,
      "hours": 1.0,
      "minutes": 0.0,
      "seconds": 0.0,
      "microseconds": 0.0,
      "milliseconds": 0.0
    }
  }
}"""
    )


@pytest.fixture()
def out_message_get_time_interval(default_asn_id):
    return OutMessage(
        default_asn_id,
        RabbitActionType.GET_TIME_INTERVAL,
        CheckFrequencyWithTimeInterval(
            CheckFrequency.LOW, TimeInterval(hours=1)
        ),
    )


@pytest.fixture()
def out_str_get_time_interval(default_asn_id):
    return (
        """{
  "asn_id": \""""
        f"{default_asn_id}"
        """\",
  "action": "GET_TIME_INTERVAL",
  "payload": {
    "check_frequency": "LOW",
    "interval": {
      "weeks": 0.0,
      "days": 0.0,
      "hours": 1.0,
      "minutes": 0.0,
      "seconds": 0.0,
      "microseconds": 0.0,
      "milliseconds": 0.0
    }
  }
}"""
    )


@pytest.fixture()
def out_message_whoami(default_asn_id):
    return OutMessage(
        default_asn_id,
        RabbitActionType.WHOAMI,
        IPDetails("64.233.165.139", "US", "GOOGLE, US", default_asn_id),
    )


@pytest.fixture()
def out_str_whoami(default_asn_id):
    return (
        """{
  "asn_id": \""""
        f"{default_asn_id}"
        """\",
  "action": "WHOAMI",
  "payload": {
    "ip": "64.233.165.139",
    "country_code": "US",
    "isp": "GOOGLE, US",
    "asn_id": \""""
        f"{default_asn_id}"
        """\"
  }
}"""
    )


@pytest.fixture()
def out_message_get_url(
    default_asn_id: str, default_ucds: List[URLCheckDetails]
):
    return OutMessage(default_asn_id, RabbitActionType.GET_URL, default_ucds)


@pytest.fixture()
def out_str_get_url(default_str_ucds: str):
    return (
        """{
  "asn_id": "15169",
  "action": "GET_URL",
  "payload": """
        f"{default_str_ucds}"
        """
}"""
    )


@pytest.fixture()
def out_message_delete_matching_hostname(
    default_asn_id: str, default_ucds: List[URLCheckDetails]
):
    return OutMessage(
        default_asn_id,
        RabbitActionType.DELETE_MATCHING_HOSTNAME,
        DeletedMatchingHostname("www.abayev-did-nothing-wro.ng", default_ucds),
    )


@pytest.fixture()
def out_str_delete_matching_hostname(default_str_ucds: str):
    return (
        """{
  "asn_id": "15169",
  "action": "DELETE_MATCHING_HOSTNAME",
  "payload": {
    "hostname": "www.abayev-did-nothing-wro.ng",
    "deleted": """
        f"{default_str_ucds}"
        """\
  }
}"""
    )


@pytest.fixture()
def out_message_get_all(default_asn_id, default_ucds):
    return OutMessage(
        default_asn_id, RabbitActionType.GET_ALL_URLS, default_ucds
    )


@pytest.fixture()
def out_str_get_all(default_str_ucds: str):
    return (
        """{
  "asn_id": "15169",
  "action": "GET_ALL_URLS",
  "payload": """
        f"{default_str_ucds}"
        """
}"""
    )


@pytest.fixture()
def out_message_update_all_for_high_freq(default_asn_id, default_ucds):
    return OutMessage(
        default_asn_id, RabbitActionType.UPDATE_ALL_FOR_HIGH_FREQ, default_ucds
    )


@pytest.fixture()
def out_str_update_all_for_high_freq(default_str_ucds: str):
    return (
        """{
  "asn_id": "15169",
  "action": "UPDATE_ALL_FOR_HIGH_FREQ",
  "payload": """
        f"{default_str_ucds}"
        """
}"""
    )


@pytest.fixture()
def out_message_update_all_for_low_freq(default_asn_id, default_ucds):
    return OutMessage(
        default_asn_id, RabbitActionType.UPDATE_ALL_FOR_LOW_FREQ, default_ucds
    )


@pytest.fixture()
def out_str_update_all_for_low_freq(default_str_ucds: str):
    return (
        """{
  "asn_id": "15169",
  "action": "UPDATE_ALL_FOR_LOW_FREQ",
  "payload": """
        f"{default_str_ucds}"
        """
}"""
    )
