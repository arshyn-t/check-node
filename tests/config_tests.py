import logging
from dataclasses import fields
from typing import Dict

import pytest
from check_node.config import get_env, Env


def test_env_dict_ok(env_dict_ok: Dict[str, str]):
    env = get_env(env_dict_ok)
    assert env.RABBIT_URL == "lol"
    assert env.DEBUG == "True"
    assert env.is_debug()
    assert env.log_level() == logging.DEBUG


def test_env_dict_fail(env_dict_fail: Dict[str, str]):
    with pytest.raises(SystemExit):
        import os

        for field in fields(Env):
            os.environ.pop(field.name)
        get_env(env_dict_fail)
