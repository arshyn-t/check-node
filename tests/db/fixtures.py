from typing import List, Tuple

import pytest

from check_node.config import DbEnv
from check_node.db.manager import DBManager, TableName
from check_node.db.models import URLCheckDetails, CheckFrequency, Scheme
from check_node.utils import get_datetime_now_by_utc


@pytest.fixture()
async def db_manager() -> DBManager:
    return DBManager()


@pytest.fixture()
async def db_manager_set_engine(db_manager: DBManager) -> DBManager:
    await db_manager.set_engine()
    return db_manager


@pytest.fixture()
async def db_manager_init_tables(
    db_manager_set_engine: DBManager
) -> DBManager:
    await db_manager_set_engine.init_tables()
    return db_manager_set_engine


@pytest.fixture()
async def db_manager_drop_all_tables(
    db_manager_init_tables: DBManager
) -> DBManager:
    await db_manager_init_tables.drop_all_tables()
    return db_manager_init_tables


@pytest.fixture()
async def db_manager_create_tables(
    db_manager_drop_all_tables: DBManager
) -> DBManager:
    await db_manager_drop_all_tables.create_tables()
    return db_manager_drop_all_tables


@pytest.fixture()
async def db_manager_inserted(
    db_manager_create_tables: DBManager,
    list_failed_of_url_check_details: List[URLCheckDetails],
    list_successful_of_url_check_details: List[URLCheckDetails],
) -> Tuple[DBManager, List[URLCheckDetails], List[URLCheckDetails]]:
    await db_manager_create_tables.insert(
        TableName.URL_CHECK_DETAILS,
        [
            *list_failed_of_url_check_details,
            *list_successful_of_url_check_details,
        ],
    )

    return (
        db_manager_create_tables,
        list_failed_of_url_check_details,
        list_successful_of_url_check_details,
    )


@pytest.fixture()
def low_and_high_clauses(db_manager_init_tables: DBManager):
    table = db_manager_init_tables.table_dict[
        TableName.URL_CHECK_DETAILS
    ].table
    return [
        table.c.check_frequency == CheckFrequency.LOW,
        table.c.check_frequency == CheckFrequency.HIGH,
    ]


@pytest.fixture()
async def db_manager_batch_insert(
    db_manager_create_tables: DBManager,
    generated_ucds_for_batch_insert: List[URLCheckDetails],
) -> Tuple[DBManager, List[URLCheckDetails]]:
    await db_manager_create_tables.insert(
        TableName.URL_CHECK_DETAILS, generated_ucds_for_batch_insert
    )
    return db_manager_create_tables, generated_ucds_for_batch_insert


@pytest.fixture()
def punycode_url_check_details() -> List[URLCheckDetails]:
    return [
        URLCheckDetails(
            "абаев-бұл-сайтты-бұғаттаған-жоқ.кз",
            Scheme.HTTP,
            "http://абаев-бұл-сайтты-бұғаттаған-жоқ.кз",
            True,
            get_datetime_now_by_utc(),
            0,
            0,
            0,
            1,
            CheckFrequency.HIGH,
            256,
            301,
        )
    ]


@pytest.fixture()
def list_successful_of_url_check_details(
    not_blocked_url_str, http_not_blocked_url_str, https_not_blocked_url_str
) -> List[URLCheckDetails]:
    return [
        URLCheckDetails(
            not_blocked_url_str,
            Scheme.HTTP,
            http_not_blocked_url_str,
            True,
            get_datetime_now_by_utc(),
            0,
            0,
            0,
            1,
            CheckFrequency.HIGH,
            256,
            301,
        ),
        URLCheckDetails(
            not_blocked_url_str,
            Scheme.HTTPS,
            https_not_blocked_url_str,
            True,
            get_datetime_now_by_utc(),
            0,
            0,
            0,
            1,
            CheckFrequency.HIGH,
            256,
            200,
        ),
    ]


@pytest.fixture()
def list_failed_of_url_check_details(
    blocked_url_str, http_blocked_url_str, https_blocked_url_str
) -> List[URLCheckDetails]:
    return [
        URLCheckDetails(
            blocked_url_str,
            Scheme.HTTP,
            http_blocked_url_str,
            False,
            get_datetime_now_by_utc(),
            0,
            0,
            0,
            30,
            CheckFrequency.LOW,
            -1,
            -1,
        ),
        URLCheckDetails(
            blocked_url_str,
            Scheme.HTTPS,
            https_blocked_url_str,
            False,
            get_datetime_now_by_utc(),
            0,
            0,
            0,
            30,
            CheckFrequency.LOW,
            -1,
            -1,
        ),
    ]


@pytest.fixture()
def fail_db_env() -> DbEnv:
    return DbEnv(
        DB_HOST="localhost",
        DB_PORT="3307",
        MYSQL_USER="tuser",
        MYSQL_PASSWORD="passwerd",
        MYSQL_ROOT_PASSWORD="smashhord",
        MYSQL_DATABASE="smashhord",
        BATCH_SIZE="7000",
    )
